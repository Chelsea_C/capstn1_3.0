﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AppsButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Activate);
        AppManager.instance.appsButton = this.GetComponent<Button>();
    }

    void Activate()
    {
        //AppManager.instance.ShowOpenApps();
        AppManager.instance.GoToPreviousApp();
    }
}
