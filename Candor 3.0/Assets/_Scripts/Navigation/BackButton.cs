﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BackButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Activate);
        AppManager.instance.backButton = this.GetComponent<Button>();
    }

    void Activate()
    {
        AppManager.instance.currentApp.GoToPreviousElement();
    }
}
