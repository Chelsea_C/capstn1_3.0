﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NavigationElement : MonoBehaviour
{
    public bool IsAppRoot = false;
    public NavigationElement previousElement;
    public bool unloadPrevious = true;
    [HideInInspector] public Loader loader;

    bool hasLoaderComponent = false;
    bool loaded;

    public bool HasLoader { get { return hasLoaderComponent; } }
    public bool IsLoaded { get { return loaded; } }

    void Awake()
    {
        loader = GetComponent<Loader>();

        if (loader == null)
            hasLoaderComponent = false;
        else
            hasLoaderComponent = true;

        loaded = false;
    }
    
    public void Unload()
    {
        loaded = false;
        if (HasLoader)
            loader.Unload();
        else
            Debug.Log(name + " does not have a Loader.");
    }

    public void Load()
    {
        loaded = true;
        if (HasLoader)
            loader.Load();
        else
            Debug.Log(name + " does not have a Loader.");
    }

}
