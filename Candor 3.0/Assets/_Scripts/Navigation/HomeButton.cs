﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class HomeButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Activate);
        AppManager.instance.homeButton = this.GetComponent<Button>();
    }

    void Activate()
    {
        AppManager.instance.GoToHome();
    }
}
