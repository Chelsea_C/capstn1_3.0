﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AudioManager : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    #region Singleton
    public static AudioManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    [Header("Audio Sources")]
    public AudioSource bgm;
    public AudioSource click;
    public AudioSource notif;
    public AudioSource vibrate;

    [Header("Background Music")]
    public AudioClip bgmClip;

    [Header("Mouse Click")]
    public AudioClip clickClip;

    [Header("Notification")]
    public AudioClip notifClip;
    public AudioClip vibrateClip;
    
    bool dragging;

    public float defaultBGMVolume;

    void Start()
    {
        PairAudioClips();
        bgm.volume = defaultBGMVolume;
        bgm.loop = true;
        if(GameManager.instance.STATE == GameState.MENU)
            bgm.Play();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
                click.Play();
    }

    public void OnBeginDrag(PointerEventData data)
    {
        dragging = true;
    }

    public void OnDrag(PointerEventData data)
    {
        dragging = true;
    }
    
    public void OnEndDrag(PointerEventData data)
    {
        dragging = false;
    }

    void PairAudioClips()
    {
        bgm.clip = bgmClip;
        click.clip = clickClip;
        notif.clip = notifClip;
        vibrate.clip = vibrateClip;
    }

    public void BGMMute(bool mute)
    {
        bgm.mute = mute;
    }

    public void SFXMute(bool mute)
    {
        click.mute = mute;
        notif.mute = mute;
    }
}
