﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioController
{
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;
        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }
        audioSource.Stop();
    }

    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime, float MinVolume)
    {
        MinVolume = Mathf.Clamp(MinVolume, 0, 1);

        float startVolume = audioSource.volume;
        while (audioSource.volume > MinVolume)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }
        audioSource.Stop();
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        audioSource.Play();
        audioSource.volume = 0f;
        while (audioSource.volume < 1)
        {
            audioSource.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime, float MaxVolume)
    {
        MaxVolume = Mathf.Clamp(MaxVolume, 0, 1);

        audioSource.Play();
        audioSource.volume = 0f;
        while (audioSource.volume < MaxVolume)
        {
            audioSource.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime, bool CurrentVolume)
    {
        float MaxVolume = 1;
        if (CurrentVolume)
            MaxVolume = audioSource.volume;

        audioSource.Play();
        audioSource.volume = 0f;
        while (audioSource.volume < MaxVolume)
        {
            audioSource.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
    }
}