﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Background : MonoBehaviour
{
    public Image image;
 
    void Awake()
    {
        LevelManager.instance.bg = this;
        image = GetComponent<Image>();
        if (image == null)
            Debug.LogError(name + ": Image component not set.");
    }

    public void SetBackgroundImage(Sprite _sprite)
    {
        image.sprite = _sprite;
    }
}
