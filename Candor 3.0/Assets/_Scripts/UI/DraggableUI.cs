﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableUI : UIBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    // draggable RectTransform
    //if null the transform this component is attached to is used
    public RectTransform dragObject;

    //the area in which we are able to move the dragObject around
    //if null, the canvas is used
    public RectTransform dragArea;

    public float xOffset = 20;
    public float yOffset = 20;

    public bool clampToArea = true;
    public bool stickyEdges = true;
    public float stickingSpeed;
    public LeanTweenType easeType;

    Vector2 originalLocalPointerPosition;
    Vector3 originalPanelLocalPosition;

    RectTransform dragObjectInternal
    {
        get
        {
            if (dragObject == null)
                return (transform as RectTransform);
            else
                return dragObject;
        }
    }
    
    RectTransform dragAreaInternal
    {
        get
        {
            if (dragArea == null)
            {
                RectTransform canvas = transform as RectTransform;
                while (canvas.parent != null && canvas.parent is RectTransform)
                {
                    canvas = canvas.parent as RectTransform;
                }
                return canvas;
            }
            else
                return dragArea;
        }
    }

    public void OnBeginDrag(PointerEventData data)
    {
        originalPanelLocalPosition = dragObjectInternal.localPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(dragAreaInternal, data.position,
            data.pressEventCamera, out originalLocalPointerPosition);


    }

    public void OnDrag(PointerEventData data)
    {
        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(dragAreaInternal, data.position,
            data.pressEventCamera, out localPointerPosition))
        {
            Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
            dragObjectInternal.localPosition = originalPanelLocalPosition + offsetToOriginal;
        }

        ClampToArea();

    }

    public void OnEndDrag(PointerEventData data)
    {
        Debug.Log("ended drag");
        MoveToEdges();
    }

    // Clamp panel to dragArea
    void ClampToArea()
    {
        if (!clampToArea)
            return;

        Vector3 pos = dragObjectInternal.localPosition;

        Vector3 minPosition = dragAreaInternal.rect.min - dragObjectInternal.rect.min;
        Vector3 maxPosition = dragAreaInternal.rect.max - dragObjectInternal.rect.max;

        pos.x = Mathf.Clamp(dragObjectInternal.localPosition.x, minPosition.x - xOffset, maxPosition.x + xOffset);
        pos.y = Mathf.Clamp(dragObjectInternal.localPosition.y, minPosition.y - yOffset, maxPosition.y + yOffset);

        dragObjectInternal.localPosition = pos;
    }

    // Move panel to edges
    void MoveToEdges()
    {
        if (!stickyEdges)
            return;

        Vector3 pos = dragObjectInternal.localPosition;

        Vector3 minPosition = dragAreaInternal.rect.min - dragObjectInternal.rect.min;
        Vector3 maxPosition = dragAreaInternal.rect.max - dragObjectInternal.rect.max;

        float xMid = dragAreaInternal.rect.size.x / 2;
        if (dragObjectInternal.localPosition.x > xMid)
            pos.x = maxPosition.x + xOffset;
        if (dragObjectInternal.localPosition.x <= xMid)
            pos.x = minPosition.x - xOffset;

        //dragObjectInternal.localPosition = pos;
        LeanTween.moveLocal(dragObject.gameObject, pos, stickingSpeed).setEase(easeType);
    }

}
