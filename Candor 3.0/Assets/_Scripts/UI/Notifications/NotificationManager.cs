﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour
{
    #region Singleton
    public static NotificationManager instance;
    void Awake()
    {
    instance = this;
    }
    #endregion

    public GameObject notifPrefab;
    public Transform notifParentTransform;
    public Sprite settings_sprite;

    public List<NotificationUI> notifications;

    void Start()
    {
        notifications.Clear();
        CreateNotification(MailManager.instance.app, "You have unread mail");
    }

    public void CreateNotification(App app, string content)
    {
        Debug.Log("Creating notif!");
        Instantiate(notifPrefab, notifParentTransform);
        NotificationUI ui = notifPrefab.GetComponent<NotificationUI>();
        ui.app = app;
        ui.appLogo.sprite = app.logo;
        ui.appName.text = app.name;
        ui.content.text = content;
        ui.transform.SetAsFirstSibling();
        notifications.Add(ui);

    }

    public void RemoveNotification(NotificationUI ui)
    {
        if (ui == null)
            return;

        notifications.Remove(ui);
        Destroy(ui.gameObject);
    }

    void Update()
    {

    }

    public void EndLevel()
    {
        for (int i = 0; i < notifications.Count; i++)
            Destroy(notifications[i].gameObject);

        notifications.Clear();
    }

    public void PromptEndLevel()
    {
        Instantiate(notifPrefab, notifParentTransform);
        NotificationUI ui = notifPrefab.GetComponent<NotificationUI>();
        ui.app = null;
        ui.GetComponent<Button>().onClick.RemoveAllListeners();
        ui.GetComponent<Button>().onClick.AddListener(OpenSettings);
        ui.appLogo.sprite = settings_sprite;
        ui.appName.text = "System";
        ui.content.text = "Click here to continue.";
        ui.transform.SetAsFirstSibling();
    }

    public void OpenSettings()
    {
        Settings.instance.Open();
    }

}
