﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationParent : MonoBehaviour
{
    void Awake()
    {
        NotificationManager.instance.notifParentTransform = this.transform;
    }
}
