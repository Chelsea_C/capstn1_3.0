﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class NotificationUI : MonoBehaviour
{
    public App app;
    public Image appLogo;
    public TMP_Text appName;
    public TMP_Text content;

    public Button close;
    public Button openApp;

    void Start()
    {
        openApp = GetComponent<Button>();
        openApp.onClick.AddListener(LaunchApp);

        close.onClick.AddListener(Close);
    }

    void Close()
    {
        NotificationManager.instance.RemoveNotification(this);
    }

    void LaunchApp()
    {
        if(app == null)
        {
            foreach (App a in AppManager.instance.apps)
                if (a.name == appName.text)
                {
                    AppManager.instance.OpenApp(a);
                    return;
                }
        }
        AppManager.instance.OpenApp(app);
        Close();
    }

}
