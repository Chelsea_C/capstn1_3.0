﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class Settings : MonoBehaviour
{
    public static Settings instance;

    public Button forwardButton;
    public Button closeButton;
    public Button icon;

    CanvasGroup cg;

    void Awake()
    {
        instance = this;
        cg = GetComponent<CanvasGroup>();
        closeButton.onClick.AddListener(Close);
        forwardButton.onClick.AddListener(FastForward);
        icon.onClick.AddListener(Open);

        AppManager.instance.settings = this;
        Close();
    }

    public void Close()
    {
        cg.alpha = 0;
        cg.interactable = false;
        cg.blocksRaycasts = false;
    }

    public void Open()
    {
        cg.alpha = 1;
        cg.interactable = true;
        cg.blocksRaycasts = true;
    }

    public void FastForward()
    {
        TimeManager.instance.ForceLevelEnd();
    }
}
