﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class Icon : MonoBehaviour
{
    public string appName;
    public App app;

    public void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OpenApp);
    }

    public void OpenApp()
    {
        Debug.Log("Opening: " + appName);
        AppManager.instance.OpenApp(app);
    }
}
