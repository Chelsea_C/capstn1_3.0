﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public abstract class AppBubble : MonoBehaviour
{
    public new string name;
    public App app;

    protected Button button;
    
    public void Awake()
    {
        if (name == "" || app == null)
        {
            Debug.LogError(name + " is missing references.");
        }

        button = GetComponent<Button>();
        AppManager.instance.appBubbles.Add(this);
    }

    public void Update()
    {
        HideWhenAppActive();
    }

    public void HideBubble()
    {
        LeanTween.alpha(gameObject, 0, 0);
        gameObject.SetActive(false);
    }

    public void HideWhenAppActive()
    {
        var canvas = GetComponent<CanvasGroup>();
        if (AppManager.instance.currentApp == app)
        {
            canvas.alpha = 0;
            canvas.interactable = false;
            canvas.blocksRaycasts = false;

        }
        else
        {
            canvas.alpha = 1;
            canvas.interactable = true;
            canvas.blocksRaycasts = true;
        }
    }

}
