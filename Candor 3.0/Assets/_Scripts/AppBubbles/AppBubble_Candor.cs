﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppBubble_Candor : AppBubble
{
    bool isActive = false;
    public Image generalOverlay;

    void Start()
    {
        button.onClick.AddListener(Activate);
        isActive = false;
    }

    new void Update()
    {
        base.Update();
    }

    public void Activate()
    {

        LeanTween.alpha(gameObject, 0, 0);

        if (AppManager.instance.currentApp == Hooter.instance.gameObject.GetComponent<App>())
        {
            generalOverlay.enabled = false;
        }
        else
        {
            generalOverlay.enabled = true;
        }

        isActive = true;
        Hooter.instance.OnCandorBubbleActivate();
    }

    public void Deactivate()
    {
        isActive = false;
        Hooter.instance.OnCandorBubbleDeactivate();
    }
}
