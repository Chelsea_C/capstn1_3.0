﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


[RequireComponent(typeof(TMP_Text))]
public class TimeDisplay : MonoBehaviour
{
    TMP_Text display;

    void Start()
    {
        display = GetComponent<TMP_Text>();
    }

    void Update()
    {
        display.text = TimeManager.instance.DISPLAY_TEXT;
    }
}
