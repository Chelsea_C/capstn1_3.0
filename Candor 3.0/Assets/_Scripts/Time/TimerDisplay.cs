﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class TimerDisplay : MonoBehaviour
{
    TMP_Text display;

    void Start()
    {
        display = GetComponent<TMP_Text>();
    }

    void Update()
    {
        if (TimeManager.instance.IsTimerActive)
            display.text = TimeManager.instance.TIMER_DISPLAY_TEXT;
        else
            display.text = "--:--";

    }
}
