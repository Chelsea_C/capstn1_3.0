﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    #region Singleton
    public static TimeManager instance;

    void Awake()
    {
        instance = this;
        //TIMER = GetComponent<Timer>();
    }
    #endregion

    public bool IsPaused = false;
    public int HOUR;
    public int MINUTE;
    public int SECOND;
    public string DISPLAY_TEXT;
    public string DISPLAY_TEXT_SECONDS;
    
    public long countdownValue = 120;
    public long currCountdownValue;
    public bool IsTimerActive = false;
    public string TIMER_DISPLAY_TEXT;

    float counter;

    public void Start()
    {
        counter = 0f;
    }

    public void Update()
    {
        if (IsPaused)
            return;

        counter += Time.deltaTime;

        UpdateTimeDisplay();

        if(IsTimerActive)
            UpdateTimerDisplay();

        if(currCountdownValue <= 0 && IsTimerActive)
        {
            StopAndResetTimer();
            LevelManager.instance.EndLevel();
        }

    }

    void UpdateTimeDisplay()
    {
        if (counter > SECOND)
        {
            SECOND++;
        }

        if (SECOND >= 59)
        {
            MINUTE++;
            SECOND = 0;
            counter = 0;
        }

        if (MINUTE >= 59)
        {
            HOUR++;
            MINUTE = 0;
        }

        if (HOUR > 12)
        {
            HOUR = 1;
        }

        string str_hour = LeadingZero(HOUR);
        string str_minute = LeadingZero(MINUTE);
        string str_second = LeadingZero(SECOND);

        DISPLAY_TEXT = str_hour + ":" + str_minute;// + ":" + str_second;
        DISPLAY_TEXT_SECONDS = str_hour + ":" + str_minute + ":" + str_second;
    }

    void UpdateTimerDisplay()
    {

        var elapsedMins = currCountdownValue / 60;
        var elapsedSecs = currCountdownValue % 60;

        string timeText = string.Format("{0:D1}:{1:D2}", elapsedMins, elapsedSecs);
        TIMER_DISPLAY_TEXT = timeText;
    }

    public void StartTimer()
    {
        IsTimerActive = true;
        StartCoroutine(StartCountdown());
    }

    public void StopAndResetTimer()
    {
        StopAllCoroutines();
        currCountdownValue = countdownValue;
        IsTimerActive = false;
    }

    public IEnumerator StartCountdown()
    {
        currCountdownValue = countdownValue;
        while(currCountdownValue > 0)
        {
            yield return new WaitForSeconds(1f);
            currCountdownValue--;
        }
    }

    string LeadingZero(int n)
    {
        return n.ToString().PadLeft(2, '0');
    }

    public void SetTimer(float duration)
    {
        countdownValue = (long)duration;
        currCountdownValue = countdownValue;
    }

    public void SetTimeTo(int _hour, int _minute, int _second)
    {
        HOUR = _hour;
        MINUTE = _minute;
        SECOND = _second;
    }

    public void ForceLevelEnd()
    {
        StopAndResetTimer();
        LevelManager.instance.EndLevel();
    }

}
