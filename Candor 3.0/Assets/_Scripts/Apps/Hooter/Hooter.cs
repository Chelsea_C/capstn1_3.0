﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hooter : App
{

    #region Singleton
    public static Hooter instance;

    protected new void Awake()
    {
        base.Awake();
        instance = this;
    }
    #endregion

    public GameData gameData;
    public List<HooterPostUI> currentPosts;
    //public List<HooterPostUI> activePosts;

    [Header("Panel Indices")]
    public int feedPanelIndex;
    public int menuPanelIndex;
    public int overlayPanelIndex;

    [Header("Candor Bubble")]
    public GameObject candorBubble;
    public GameObject overlay_buttons;
    public Image overlay_BG;
    public Button candorBubbleClose;
    public Button candorBubbleGoToApp;

    [Header("UI")]
    public GameObject posts;
    public GameObject postPrefab;
    public GameObject postsParent;
    public float spacing;
    
    public GameObject commentUIPrefab;

    void Start()
    {
        overlay_BG.enabled = false;
        overlay_buttons.SetActive(false);
        candorBubbleClose.onClick.AddListener(OnCandorBubbleDeactivate);
        candorBubbleGoToApp.onClick.AddListener(OnOpenCandorApp);
    }

    public void CreatePost(Article a)
    {
        GameObject post = Instantiate(postPrefab, postsParent.transform);

        HooterPostUI ui = post.GetComponent<HooterPostUI>();
        ui.AddArticle(a);

        ui.number_Likes = Random.Range(0, 100);
        ui.number_Comments = Random.Range(0, 100);
        ui.number_Rehoots = Random.Range(0, 60);

        currentPosts.Add(ui);
        ResetPostParentTransform();
    }

    public void ResetPostParentTransform()
    {
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(postsParent.GetComponent<RectTransform>());
    }

    public void UpdatePost(int id, string edit)
    {

    }

    public void EndLevel()
    {
        for(int i = 0; i < currentPosts.Count; i++)
        {
            Destroy(currentPosts[i].gameObject);
        }

        currentPosts.Clear();
    }

    public void AddComment(int _postID)
    {
        for (int i = 0; i < currentPosts.Count; i++)
        {
            if (currentPosts[i].postID == _postID)
            {

            }
        }
    }

    public void RemovePost(int _postID)
    {
        for (int i = 0; i < currentPosts.Count; i++)
            if (currentPosts[i].postID == _postID)
                currentPosts[i].Remove();
    }

    #region Candor Bubble

    public void OnCandorBubbleActivate()
    {
        overlay_BG.enabled = true;
        overlay_buttons.SetActive(true);
        candorBubble.SetActive(false);
        EnableClickablePosts();
        
    }

    public void OnCandorBubbleDeactivate()
    {
        candorBubble.SetActive(true);
        overlay_BG.enabled = false;
        overlay_buttons.SetActive(false);
        DisableClickablePosts();
    }

    public void OnOpenCandorApp()
    {
        AppManager.instance.OpenApp(Candor.instance);
        OnCandorBubbleDeactivate();
    }

    void EnableClickablePosts()
    {
        posts.GetComponent<Mask>().showMaskGraphic = false;
        foreach(HooterPostUI p in currentPosts)
        {
            p.addToCandorButton.SetActive(true);
        }
    }

    void DisableClickablePosts()
    {
        posts.GetComponent<Mask>().showMaskGraphic = true;
        foreach (HooterPostUI p in currentPosts)
        {
            p.addToCandorButton.SetActive(false);
        }
    }

    public void OpenInCandor(Article article)
    {
        if (!LevelManager.instance.IsAllowingSubmissions)
            return; 

        var candor = Candor.instance;

        //Debug.Log("Opening article with ID no. " + article.id + " in Candor");
        if (article == null)
        {
            Debug.LogError(name + " does not have an article");
            return;
        }

        candor.AddHooterPost(article);
        AppManager.instance.OpenApp(candor.GetComponent<App>());
        candor.GoToElement(Candor.instance.postsPanel);
        OnCandorBubbleDeactivate();
    }

    public void AddToCandor(Article article)
    {
        if (article == null)
        {
            Debug.LogError(name + " does not have an article");
            return;
        }
        Candor.instance.AddHooterPost(article);
    }

    #endregion

}
