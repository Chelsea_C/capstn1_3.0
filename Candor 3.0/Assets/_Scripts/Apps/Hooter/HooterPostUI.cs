﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HooterPostUI : PostUI
{
    [Header("UI - Candor Interfacing")]
    public Sprite notAddedSprite;
    public Sprite addedSprite;
    public GameObject addToCandorButton;

    [Header("UI - Reactions")]
    public TMP_Text likes;
    public TMP_Text comments;
    public TMP_Text rehoots;

    public int number_Likes = 0;
    public int number_Comments = 0;
    public int number_Rehoots = 0;

    [Header("UI - Comments")]
    public GameObject commentsParent;

    Button button;

    public void Awake()
    {
        if (addToCandorButton == null)
            Debug.LogError(name + " has no AddToCandorButton gameObject set.");
        button = addToCandorButton.GetComponent<Button>();
        button.onClick.AddListener(Activate);
    }

    public void Update()
    {
        likes.text = number_Likes.ToString();
        comments.text = number_Comments.ToString();
        rehoots.text = number_Rehoots.ToString();
    }

    void Activate()
    {
        Hooter.instance.AddToCandor(this.article);
        addToCandorButton.GetComponent<Image>().sprite = addedSprite;
        button.enabled = false;
    }

    public void Reset()
    {
        Debug.Log("Reset " + name);
        addToCandorButton.GetComponent<Image>().sprite = notAddedSprite;
        button.enabled = true;
    }
}
