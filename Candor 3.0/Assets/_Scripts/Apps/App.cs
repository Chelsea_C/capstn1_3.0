﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class App : MonoBehaviour
{
    public new string name;
    public bool IsOpen;
    public Sprite logo;

    [Header("Navigation")]
    public App previousApp;
    public List<NavigationElement> elements;
    public NavigationElement root;
    public NavigationElement currentElement;

    protected void Awake()
    {
        AppManager.instance.apps.Add(this);
        IsOpen = false;
        GetElements();
        if (root == null)
            GetRoot();
    }

    protected void Update()
    {
        foreach (NavigationElement ne in elements)
        {
            if (ne != currentElement)
                ne.gameObject.SetActive(false);
            else
                ne.gameObject.SetActive(true);
        }
    }

    public void GoToElement(NavigationElement _element)
    {
        if(currentElement != null)
            Deactivate(currentElement);
        Activate(_element);

        _element.previousElement = currentElement;
        currentElement = _element;
    }

    public void GoToPreviousElement()
    {
        var appMgr = AppManager.instance;
        if (currentElement == root)
        {
            if (appMgr.currentApp.previousApp != null)
            {
                appMgr.OpenApp(appMgr.currentApp.previousApp);
                return;
            }
            
            AppManager.instance.GoToHome();
            return;
        }

        if (currentElement.previousElement == null)
            AppManager.instance.GoToHome();

        ////Unload current element
        //currentElement.Unload();

        //Get previous element
        var previousElement = currentElement.previousElement;

        //Clear current element's previous element since we're going back to it already
        currentElement.previousElement = null;
        
        ////If previous element was unloaded, load it again
        //if (previousElement.IsLoaded || currentElement.unloadPrevious)
        //    previousElement.Load();

        //Set the previous element as the new current element
        currentElement = previousElement;
    }

    void GetRoot()
    {
        for (int i = 0; i < elements.Count; i++)
        {
            if (elements[i].IsAppRoot)
            {
                root = elements[i];
                return;
            }
        }

        if (root == null)
            Debug.LogError("No root element assigned for " + name);
    }

    void GetElements()
    {
        NavigationElement[] nElements = GetComponentsInChildren<NavigationElement>();
        foreach(NavigationElement ne in nElements)
            elements.Add(ne);
    }

    void Activate(NavigationElement element)
    {
        element.gameObject.SetActive(true);
        //element.Load();
    }

    void Deactivate(NavigationElement element)
    {
        //element.Unload();
        element.gameObject.SetActive(false);
    }
}
