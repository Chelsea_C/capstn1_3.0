﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    #region Singleton
    public static AppManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    [Header("Navigation Buttons")]
    public Button homeButton;
    public Button appsButton;
    public Button backButton;
    public GameObject home;
    public GameObject appsList;

    [Header("Apps")]
    public List<App> apps;
    public List<AppBubble> appBubbles;
    public Settings settings;
    
    public App currentApp;
    public List<App> displayOrder;

    bool GoingToPrevious = false;

    void Start()
    {
        currentApp = null;
        foreach (App a in apps)
            a.GoToElement(a.root);
    }

    void Update()
    {
        DisplayCurrentApp();

        bool appIsActive;
        if (currentApp == null)
            appIsActive = false;
        else
            appIsActive = true;

        home.SetActive(!appIsActive);
    }

    void DisplayCurrentApp()
    {
        for (int i = 0; i < apps.Count; i++)
        {
            if (apps[i] == currentApp)
            {
                apps[i].gameObject.SetActive(true);
            }
            else
                apps[i].gameObject.SetActive(false);
        }
    }

    //public void SetCurrentApp(App newActive)
    //{
    //    if(currentApp != null)
    //        displayOrder.Add(currentApp);
        
    //    if (currentApp != null)
    //        CloseApp();

    //    currentApp = newActive;
    //}

    public void OpenApp(App _app)
    {
        settings.Close();

        //If app is already open, bring to front
        if (_app.IsOpen)
            BringToFront(_app);
        else
        {
            _app.IsOpen = true;
            BringToFront(_app);
            _app.GoToElement(_app.root);
        }

        //If not going to previous, record current app as new previous
        //if(!GoingToPrevious)
            _app.previousApp = currentApp;

        currentApp = _app;

    }

    public void GoToPreviousApp()
    {
        GoingToPrevious = true;

        if (currentApp == null)
        {
            OpenApp(home.GetComponent<Home>().lastOpened);
            GoingToPrevious = false;
            return;
        }

        if (currentApp.previousApp == null)
        {
            home.GetComponent<Home>().lastOpened = currentApp;
            GoToHome();
            GoingToPrevious = false;
            return;
        }

        OpenApp(currentApp.previousApp);
        
    }

    public void CloseApp(App _app)
    {
        _app.IsOpen = false;

        foreach (NavigationElement ne in _app.elements)
            ne.previousElement = null;
    }

    public void SetHome(GameObject go)
    {
        home = go;
    }

    void Hide(App _app)
    {
        _app.GetComponent<Canvas>().sortingOrder = -5;
    }

    void BringToFront(App _app)
    {
        _app.GetComponent<Canvas>().sortingOrder = 5;
    }

    public void GoBack()
    {
        List<App> copy = new List<App>();
        for(int i = 0; i < displayOrder.Count; i++)
        {
            if (i != displayOrder.Count - 1)
                copy.Add(displayOrder[i]);
        }

        App previousApp = displayOrder[displayOrder.Count - 1];
        previousApp.GoToElement(previousApp.root);
        currentApp = previousApp;
        //SetcurrentApp(previousApp);

        displayOrder.Clear();
        displayOrder.AddRange(copy);
    }

    public void GoToHome()
    {
        if (currentApp != null)
        {
            home.GetComponent<Home>().lastOpened = currentApp;
            CloseApp(currentApp);
            currentApp = null;
        }

        foreach (App a in apps)
            a.previousApp = null;
        
        home.SetActive(true);
    }

    public void CloseAndResetAll()
    {
        foreach (App a in apps)
            a.GoToElement(a.root);

        settings.Close();
        GoToHome();
    }

}
