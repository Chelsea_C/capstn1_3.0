﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Home : MonoBehaviour
{
    public App lastOpened;
    void Awake()
    {
        AppManager.instance.SetHome(this.gameObject);    
    }
}
