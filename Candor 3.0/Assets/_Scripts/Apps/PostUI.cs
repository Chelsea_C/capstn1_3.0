﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PostUI : MonoBehaviour
{
    public Article article;
    public int postID;
    public Author author;

    [Header("UI - Main")]
    public Image authorPortrait;
    public TMP_Text authorName;
    public TMP_Text authorTag;
    public TMP_Text timeElapsed;
    public TMP_Text content;
    //public TMP_Text edited;


    public void AddArticle(Article _article)
    {
        article = _article;
        postID = _article.id;

        author = AuthorManager.instance.GetAuthor(_article.author);
        authorPortrait.sprite = author.portrait;
        authorName.text = author.name;
        authorTag.text = "@" + author.name;
        authorTag.text = authorTag.text.Replace(" ", string.Empty);

        int rand = Random.Range(0, 60);
        timeElapsed.text = rand + " mins";


        content.text = _article.content;
        content.text = content.text.Replace("%", "'");

        AddSources();
    }

    public void AddSources()
    {
        if (article.source == null)
            return;

        string sources = "$$Source: ";
        sources += article.source.text + " ";
        content.text += sources;
        content.text = content.text.Replace('$', '\n');
    }

    public void Remove()
    {
        Destroy(gameObject);
    }

}
