﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class CandorEvaluatedPostUI : MonoBehaviour
{
    public PlayerReview review;
    Article article;

    [Header("UI - Post")]
    public Image authorPortrait;
    public TMP_Text authorName;
    public TMP_Text authorTag;
    public TMP_Text timeElapsed;
    public TMP_Text content;

    [Header("Template - Evaluation")]

    public Image didItMatch;
    public Sprite match_correct;
    public Sprite match_incorrect;
    
    public Color32 correct_color;
    public Sprite correct_sprite;
    public string correct_match = "matched the majority of reviews.";
    public string correct_phrase = "Keep it up!";

    public Color32 incorrect_color;
    public Sprite incorrect_sprite;
    public string incorrect_match = "did not match the majority of reviews.";
    public string incorrect_phrase = "Do better next time!";

    [Header("UI - Candor Evaluation")]// Candor Info Rating
    public TMP_Text c_info_rating;
    public TMP_Text c_source_rating;

    [Header("UI - Player Evaluation")]
    public TMP_Text p_info_rating;
    public TMP_Text p_source_rating;
    public Image p_source_mark;
    public Image p_info_mark;



    public void AddReview(PlayerReview _review)
    {
        review = _review;
        article = _review.article;
        AddArticle(article);

        SetCandorReview();
        SetPlayerReview();
    }

    void AddArticle(Article _article)
    {
        var author = AuthorManager.instance.GetAuthor(_article.author);
        authorPortrait.sprite = author.portrait;
        authorName.text = author.name;
        authorTag.text = "@" + author.name;
        authorTag.text = authorTag.text.Replace(" ", string.Empty);

        int rand = Random.Range(0, 60);
        timeElapsed.text = rand + " mins";


        content.text = _article.content;
        content.text = content.text.Replace("%", "'");

        if (article.source == null)
            return;

        string sources = "$$Source: ";
        sources += article.source.text + " ";
        content.text += sources;
        content.text = content.text.Replace('$', '\n');
    }

    void SetMark(Image _image, bool correct)
    {
        if (correct)
        {
            _image.sprite = correct_sprite;
            _image.color = correct_color;
        }
        else
        {
            _image.sprite = incorrect_sprite;
            _image.color = incorrect_color;
        }
    }

    void SetPlayerReview()
    {
        if (review == null)
            return;
        
        p_info_rating.text = RatingManager.instance.GetText(review.infoRating);
        p_source_rating.text = RatingManager.instance.GetText(review.sourceRating);

        didItMatch.sprite = match_incorrect;

        switch(review.RESULT)
        {
            
            case ReviewResult.CORRECT_RATINGS:
                {
                    SetMark(p_info_mark, true);
                    SetMark(p_source_mark, true);
                    didItMatch.sprite = match_correct;
                    break;
                }
            case ReviewResult.WRONG_INFO_RATING:
                {
                    SetMark(p_info_mark, false);
                    SetMark(p_source_mark, true);
                    break;
                }
            case ReviewResult.WRONG_SOURCE_RATING:
                {
                    SetMark(p_info_mark, true);
                    SetMark(p_source_mark, false);
                    break;
                }
            case ReviewResult.WRONG_RATINGS:
                {
                    SetMark(p_info_mark, false);
                    SetMark(p_source_mark, false);
                    
                    break;
                }
            default:
                break;
        }

    }

    void SetCandorReview()
    {
        if (article == null)
            return;

        c_info_rating.text = RatingManager.instance.GetText(article.INFO_RATING);
        c_source_rating.text = RatingManager.instance.GetText(article.SOURCE_RATING);
    }

}
