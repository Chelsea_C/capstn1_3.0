﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Tab
{
    public string name;
    public Button button;
    public GameObject container;
    public GameObject prefab;
}


public class CandorSidemenuUI : MonoBehaviour
{
    public Tab activeTab;
    public ScrollRect scrollrect;
    public List<Tab> tabs;
    
    void Awake()
    {
        foreach(Tab t in tabs)
        {
            t.button.onClick.AddListener(delegate { Activate(t); } );
        }

        activeTab = null;
    }

    void Update()
    {
        foreach(Tab t in tabs)
        {
            if (activeTab == t)
            {
                t.container.SetActive(true);
                scrollrect.content = t.container.GetComponent<RectTransform>() ;
            }
            else
            {
                t.container.SetActive(false);
            }
        }
    }

    void Activate(Tab tab)
    {
        activeTab = tab;
    }

    public void AddValidatedSource(Source source)
    {
        Tab validatedSources = null;
        foreach (Tab t in tabs)
            if (t.name == "Validated Sources")
                validatedSources = t;

        GameObject go = Instantiate(validatedSources.prefab, validatedSources.container.transform);
        go.GetComponentInChildren<TMP_Text>().text = source.text;
    }

    public void AddRule(Rule rule)
    {
        Tab rules = null;
        foreach (Tab t in tabs)
            if (t.name == "Validated Sources")
                rules = t;

        GameObject go = Instantiate(rules.prefab, rules.container.transform);
        go.GetComponentInChildren<TMP_Text>().text = rule.description;
    }

}
