﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class CandorPostUI : PostUI
{

    [Header("UI - Activator")]
    public Button postButton;
    public Button postDisplayButton;

    [Header("UI - Decision-making")]
    public Button submitButton;
    public List<GameObject> inputsParents;
    public TMP_Dropdown info_dropDown;
    public TMP_Dropdown source_dropDown;
    public bool submitted;
    public InfoRating infoRating;
    public SourceRating sourceRating;

    [Header("UI - Submitted")]
    public GameObject submittedPanel;
    public TMP_Text submitted_infoRating;
    public TMP_Text submitted_sourceRating;


    [Header("Colors")]
    public Image postBackground;
    public Color32 undecidedColor;
    public Color32 approvedColor;
    public Color32 rejectedColor;
    
    Button postActivator;

    public void Start()
    {
        submitted = false;
        submittedPanel.SetActive(false);
        
        if (postBackground == null)
            postBackground = GetComponent<Image>();

        if (postBackground != null)
            postBackground.color = undecidedColor;
        else
            Debug.Log(name + ": Post background not set");

        postActivator = GetComponent<Button>();
        postActivator.onClick.AddListener(Activate);

        EnableInputs();
        DisableActivation();
        //EnableActivation();
    }

    void Activate()
    {
        Candor.instance.SetActivePost(this);

        EnableInputs();
        DisableActivation();
    }

    public void RateInformation()
    {
        switch(info_dropDown.value)
        {
            case 0: //FALSE
                infoRating = InfoRating.FALSE;
                break;
            case 1: //TRUE
                infoRating = InfoRating.TRUE;
                break;
            case 2: //MIXTURE
                infoRating = InfoRating.MIXTURE;
                break;
            case 3: //UNPROVEN
                infoRating = InfoRating.UNPROVEN;
                break;
            default: //NULL
                infoRating = InfoRating.NULL;
                break;
        }
    }

    public void RateSource()
    {
        switch(source_dropDown.value)
        {
            case 0: //UNCITED
                sourceRating = SourceRating.UNCITED;
                break;
            case 1: //VALIDATED
                sourceRating = SourceRating.VALIDATED;
                break;
            case 2: //NONVALIDATED
                sourceRating = SourceRating.NONVALIDATED;
                break;
            default: //NULL
                sourceRating = SourceRating.NULL;
                break;
        }
    }

    public void SubmitReview()
    {
        RateInformation();
        RateSource();

        submitted = true;
        Candor.instance.SubmitPostReview(this.article, infoRating, sourceRating);

        DisableInputs();
        DisplaySubmission();
    }

    public void RemovePost()
    {
        for (int i = 0; i < Hooter.instance.currentPosts.Count; i++)
        {
            if (Hooter.instance.currentPosts[i].article.id == article.id)
            {
                Hooter.instance.currentPosts[i].Reset();
            }
        }

        Destroy(this.gameObject);
    }

    public void EnableInputs()
    {
        if (submitted)
            return;

        foreach (GameObject go in inputsParents)
            go.SetActive(true);
    }

    void DisplaySubmission()
    {
        if (!submitted)
            return;

        submittedPanel.SetActive(true);
        submitted_infoRating.text = "Information: " + RatingManager.instance.GetText(infoRating);
        submitted_sourceRating.text = "Source: " + RatingManager.instance.GetText(sourceRating);
    }

    public void DisableInputs()
    {
        foreach (GameObject go in inputsParents)
            go.SetActive(false);
    }

    public void EnableActivation()
    {
    //    if (Candor.instance.activePost == this)
    //        return;
        postButton.enabled = true;
        postDisplayButton.enabled = true;
    }

    public void DisableActivation()
    {
        postButton.enabled =false;
        postDisplayButton.enabled = false;
    }

    public void Deactivate()
    {
        // if not submitted with decision, go back to original color
        if (!submitted)
            postBackground.color = undecidedColor;

        DisableInputs();
        EnableActivation();
    }

}
