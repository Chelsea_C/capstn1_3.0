﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Candor : App
{
    #region Singleton
    public static Candor instance;
    protected new void Awake()
    {
        base.Awake();
        instance = this;
    }
    #endregion

    public PlayerData playerData;
    

    [Header("Panel NavigationElements")]
    public NavigationElement overviewPanel;
    public NavigationElement rulesPanel;
    public NavigationElement historyPanel;
    public NavigationElement postsPanel;

    [Header("UI - Frame - Menu Buttons")]
    public Button overviewButton;
    public Button rulesButton;
    public Button historyButton;
    public Button postsButton;

    [Header("UI - Overview Panel")]
    public Image playerPic;
    public TMP_Text playerName;
    public TMP_Text jobTitle;
    public TMP_Text totalPosts;
    public TMP_Text reviewAccuracy;
    public TMP_Text correctAmount;
    public TMP_Text incorrectAmount;
    public TMP_Text totalEarned;
    public TMP_Text rewardsAmount;
    public TMP_Text earningsLossAmount;

    [Header("UI - Rules Panel")]
    public CandorSidemenuUI info;

    [Header("UI - Posts Panel")]
    public CanvasGroup submissions_end_overlay;
    public CandorPostUI activePost;
    public GameObject candorPostUIPrefab;
    public Transform postListParent;
    public List<Article> playerReviews_Submissions;
    public GameObject postDeactivator;
    public Camera mainCamera;

    [Header("UI - Evaluations Panel")]
    public GameObject candorEvaluationUIPrefab;
    public Transform historyListParent;
    public List<CandorPostUI> level_posts;
    public List<PlayerReview> level_submissions;
    public GameObject candorRuleUIPrefab;
    public Sprite rulePassed;
    public Sprite ruleBroken;

    public void Start()
    {
        activePost = candorPostUIPrefab.GetComponent<CandorPostUI>();
        if (activePost == null)
            Debug.LogError(name + " is missing the PostUI object.");

        SetButtons();
        GenerateValidatedSourceList();
    }

    protected new void Update()
    {
        base.Update();
        UpdatePostsView();
    }

    public void EndLevel()
    {
        for (int i = 0; i < level_submissions.Count; i++)
            CreateEvaluation(level_submissions[i]);

        for (int i = 0; i < level_posts.Count; i++)
            Destroy(level_posts[i].gameObject);

        level_posts.Clear();
        level_submissions.Clear();
        UpdateOverview(); 
    }

    #region UI Buttons Setup
    void SetButtons()
    {
        overviewButton.onClick.AddListener(delegate { GetComponent<App>().GoToElement(overviewPanel); });
        historyButton.onClick.AddListener(delegate { GetComponent<App>().GoToElement(historyPanel); });
        rulesButton.onClick.AddListener(delegate { GetComponent<App>().GoToElement(rulesPanel); });
        postsButton.onClick.AddListener(delegate { GetComponent<App>().GoToElement(postsPanel); });
    }
    #endregion

    #region Overview Panel

    void UpdateOverview()
    {
        playerPic.sprite = playerData.playerPortrait;
        playerName.text = playerData.name;
        totalPosts.text = playerData.str_total_reviews;
        reviewAccuracy.text = playerData.str_review_accuracy;
        correctAmount.text = playerData.str_total_correct;
        incorrectAmount.text = playerData.str_total_incorrect;
        totalEarned.text = playerData.str_amount_rewards;
        rewardsAmount.text = playerData.str_amount_rewards;
        earningsLossAmount.text = playerData.str_amount_loss;
    }

    #endregion

    #region Rules Panel

    public void GenerateValidatedSourceList()
    {
        foreach(Source source in GameManager.instance.DATA.SOURCES)
        {
            if ((SourceRating)source.credibility == SourceRating.VALIDATED)
                info.AddValidatedSource(source);
        }
    }


    #endregion

    #region Post Panel
    public void AddHooterPost(Article _article)
    {
        GameObject post = Instantiate(candorPostUIPrefab, postListParent.transform); 
        CandorPostUI ui = post.GetComponent<CandorPostUI>();
        ui.AddArticle(_article);
        ui.content.text = _article.linkedContent;
        ui.AddSources();

        ui.content.GetComponent<ClickableText>().pCamera = mainCamera;
        ui.DisableInputs();

        level_posts.Add(ui);
    }

    void UpdatePostsView()
    {
        //// if there is no active post
        //if(activePost == null)
        //{
        //    // disable deactivators
        //    postDeactivator.SetActive(false);
        //    return;
        //}
        //postDeactivator.SetActive(true);

        if(LevelManager.instance.IsAllowingSubmissions)
        {
            submissions_end_overlay.interactable = false;
            submissions_end_overlay.blocksRaycasts = false;
        }
        else
        {
            submissions_end_overlay.interactable = true;
            submissions_end_overlay.blocksRaycasts = true;
        }
    }

    public void SetActivePost(CandorPostUI ui)
    {
        //DisableActivePost();
        activePost = ui;
    }

    public void DisableActivePost()
    {
        activePost.Deactivate();
        activePost = null;
    }

    public void RemoveActivePost()
    {
        activePost.Remove();
    }

    public void SubmitPostReview(Article _article, InfoRating _infoRating, SourceRating _sourceRating)
    {
        PlayerReview review = new PlayerReview(_article, _infoRating, _sourceRating);
        level_submissions.Add(review);
    }

    #endregion

    #region History / Evaluations Panel
    public void ClearHistory()
    {
        GameObject[] posts = historyListParent.GetComponentsInChildren<GameObject>();
        for (int i = 0; i < posts.Length; i++)
            Destroy(posts[i]);
    }

    public void CreateEvaluation(PlayerReview review)
    {
        if (review.article.HasBeenReviewed)
            return;
        review.article.HasBeenReviewed = true;

        GameObject post = Instantiate(candorEvaluationUIPrefab, historyListParent);
        CandorEvaluatedPostUI ui = post.GetComponent<CandorEvaluatedPostUI>();
        ui.AddReview(review);
    }
    #endregion
}

