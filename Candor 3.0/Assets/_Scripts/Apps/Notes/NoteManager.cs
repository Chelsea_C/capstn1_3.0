﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NoteManager : MonoBehaviour
{
    #region Singleton
    public static NoteManager instance;
    public void Awake()
    {
        instance = this;
    }
    #endregion

    public TMP_Text content;

    public void SetNoteContent(string _content)
    {
        content.text = _content;
    }

}
