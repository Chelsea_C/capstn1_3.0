﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class Note : MonoBehaviour
{
    public string noteTitleText;
    public TMP_Text noteTitle;
    public TMP_Text noteContentText;

    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(Activate);
        noteTitle.text = noteTitleText;
    }

    public void Activate()
    {
        NoteManager.instance.SetNoteContent(noteContentText.text);
    }

}
