﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MailManager : MonoBehaviour
{
    #region Singleton
    public static MailManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public Transform displayParent;
    public MailSideMenu sidemenu;
    public GameObject regular_mail_prefab;
    public GameObject candor_mail_prefab;
    public App app;

    void Start()
    {
        app = GetComponent<App>();
        CreateMail("United Bank", DateManager.instance.CURRENT_DATE.AddDays(-7), "Loan Payment", "Dear Mr. McClane, &" +
            "This serves as a reminder of your next installment of $350.00 due on " + DateManager.instance.END_DATE.ToString("dddd, dd MMMM yyyy") + ".&" +
            "Thank you for choosing United Bank, we look forward to your continued cooperation!" +
            "& & United Bank");

        CreateMail("Ernest Brown", DateManager.instance.CURRENT_DATE.AddDays(-2), "Rent Overdue", "Hey George,&" +
            "I need your payment by " + DateManager.instance.END_DATE.ToString("MMMM dd") + ".&" +
            "And you still owe last month's rent, so I'll be expecting $400.&" +
            "I do hope you're doing okay though bud.&" +
            "-Ernest");

        CreateMail("Candor, Inc.", DateManager.instance.CURRENT_DATE, "Welcome to Candor", "Hello Mr. McClane," +
            "&Congratulations once again on being accepted as a Junior Analyst for our fact-checking community!" +
            "&We value your participation in the fight against misinformation in a digital age." +
            "&You start tomorrow, first thing! You can work off the tablet that you brought to orientation for app setup." +
            "&Take the time today to click around and get further familiarized." +
            "&Best of luck to you!" +
            "&" +
            "&Candor");
    }
    
    void Update()
    {

    }

    public void CreateMail(string from, DateTime date, string subject, string content)
    {
        GameObject go_display = Instantiate(regular_mail_prefab, displayParent);

        Mail mail = go_display.GetComponent<Mail>();
        mail.SetContent(from, date, subject, content.Replace('&', '\n'));

        sidemenu.CreateMailTab(mail);
    }

    public void CreateCandorMail(DateTime date, LevelSummary summary)
    {
        GameObject go = Instantiate(candor_mail_prefab, displayParent);
        MailCandor mail = go.GetComponent<MailCandor>();
        mail.SetCandorContent(date, summary);

        sidemenu.CreateMailTab(mail);
    }
}
