﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Mail : MonoBehaviour
{
    public DateTime dateTime;
    public TMP_Text TMP_from;
    public TMP_Text TMP_to;
    public TMP_Text TMP_date;
    public TMP_Text TMP_subject;
    public TMP_Text TMP_content;

    public string from;
    public string to;
    public string date;
    public string subject;

    void Update()
    {
        TimeSpan elapsed_days = DateManager.instance.CURRENT_DATE - dateTime;

        if ((int)elapsed_days.TotalDays > 0)
        {
            TMP_date.text = dateTime.ToString("MMMM dd") + " (" + (int)elapsed_days.TotalDays + " day ago)";
            if((int)elapsed_days.TotalDays > 1)
                TMP_date.text = dateTime.ToString("MMMM dd") + " (" + (int)elapsed_days.TotalDays + " days ago)";
        }
        else
            TMP_date.text = dateTime.ToString("MMMM dd");
    }

    public void SetContent(string _from, DateTime _date, string _subject, string _content = " ")
    {
        TMP_from.text = "FROM: " + _from;
        from = _from;
        TMP_to.text = "TO: " + Player.instance.data.name;
        to = Player.instance.data.name;
        dateTime = _date;
        TMP_date.text = _date.ToString("MMMM dd");
        date = _date.ToString("MMMM dd");
        TMP_subject.text = "Subject: " + _subject;
        subject = _subject;
        TMP_content.text = _content;
    }

}
