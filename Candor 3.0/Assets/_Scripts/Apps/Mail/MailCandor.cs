﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MailCandor : Mail
{

    public TMP_Text accuracy;
    public TMP_Text accuracyFeedback;
    public TMP_Text grade;
    public TMP_Text gradeFeedback;
    public TMP_Text rewards;
    public TMP_Text rewardsFeedback;

    public TMP_Text generalFeedback;

    public void SetCandorContent(DateTime _date, LevelSummary summary)
    {
        TMP_from.text = "FROM: " + "Candor, Inc.";
        from = "Candor, Inc.";

        dateTime = _date;
        date = _date.ToString("MMMM dd");
        TMP_to.text = "TO: " + Player.instance.data.name;
        to = Player.instance.data.name;

        TMP_subject.text = "Subject: " + "Submissions Summary";
        subject = "Submissions Summary";
        grade.text = summary.GRADE.letter_text;
        accuracy.text = summary.FINAL_ACCURACY.ToString("F1") + "%";
        rewards.text = "$" + summary.REWARDS_EARNED.ToString("F2");

        if(summary.SUBMISSION_ACCURACY < GameManager.instance.END_ACCURACY)
        {
            generalFeedback.text = "WARNING: Your accuracy fell below the minimum 60%. Candor values accuracy above all else. " +
                "Failing to meet the minimum again will result in termination. Consider this your only warning. Best of luck!";
        }
        else
        {
            generalFeedback.text = "Keep up the good work!";
        }

        accuracyFeedback.text = "This is the amount of reviews you got correctly " +
            "in comparison to the Candor community standards) " +
            "out of your total reviews for this submission period (not overall).";
        gradeFeedback.text = summary.GRADE.feedback_text;
        rewardsFeedback.text = "Members receive a base reward of $" + LevelManager.instance.currentLevelInfo.REWARD_PER_REVIEW.ToString("F2") +
            "per review, and a bonus multiplier depending on their submissions grade.";
    }
}
