﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class MailTab : MonoBehaviour
{
    public TMP_Text TMP_from;
    public TMP_Text TMP_subject;
    public TMP_Text TMP_date;

    public Button button;
    public Mail mail;

    public void AddMail(Mail _mail)
    {
        mail = _mail;
        TMP_from.text = _mail.from;
        TMP_subject.text = _mail.subject;
        TMP_date.text = _mail.date;
    }
}
