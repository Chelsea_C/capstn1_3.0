﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MailSideMenu : MonoBehaviour
{
    public MailTab activeMailTab;
    public ScrollRect mailDisplayScrollrect;
    public List<MailTab> mailTabs;
    public Transform tabParent;
    public GameObject mailTabPrefab;
    public Color32 activeColor;
    public Color32 inactiveColor;

    void Awake()
    {
        foreach (MailTab m in mailTabs)
        {
            m.button.onClick.AddListener(delegate { Activate(m); });
        }

        activeMailTab = null;
    }

    void Update()
    {
        foreach (MailTab m in mailTabs)
        {
            if (activeMailTab == m)
            {
                m.button.image.color = activeColor;
                m.mail.gameObject.SetActive(true);
                mailDisplayScrollrect.content = m.mail.gameObject.GetComponent<RectTransform>();
            }
            else
            {
                m.button.image.color = inactiveColor;
                m.mail.gameObject.SetActive(false);
            }
        }
    }

    public void CreateMailTab(Mail mail)
    {
        GameObject tab = Instantiate(mailTabPrefab, tabParent);

        MailTab mt = tab.GetComponent<MailTab>();
        mt.AddMail(mail);
        mt.button = tab.GetComponent<Button>();
        mt.button.onClick.AddListener(delegate { Activate(mt); });

        mt.transform.SetAsFirstSibling();

        mailTabs.Add(mt);
    }

    public void Activate(MailTab tab)
    {
        activeMailTab = tab;
    }
}
