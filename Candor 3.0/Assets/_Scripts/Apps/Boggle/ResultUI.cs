﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DATABASES;
using TMPro;

public class ResultUI : MonoBehaviour
{
    public TMP_Text heading;
    public TMP_Text content;
    public TMP_Text url;
    public Image logo;

    [SerializeField]
    DatabaseEntry entry;

    public void AddEntry(DatabaseEntry _entry)
    {
        entry = _entry;
        heading.text = entry.heading;
        content.text = entry.content;
        url.text = entry.url;
        logo.sprite = entry.logo;
    }
}
