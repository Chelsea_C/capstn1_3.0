﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DATABASES;
using System;


public class Boggle : App
{
    #region Singleton
    public static Boggle instance;

    protected new void Awake()
    {
        base.Awake();
        instance = this;
    }
    #endregion

    [Header("Database")]
    public GameData database;
    public List<DatabaseEntry> results;
    
    [Header("UI")]
    public TMP_InputField inputField_search;
    public TMP_InputField inputField_results;
    public Scrollbar scrollbar;
    public GameObject resultPrefab;
    public Transform resultsParent;
    public GameObject errorScreen;


    [Header("Panels")]
    public NavigationElement searchPanel;
    public NavigationElement resultsPanel;

    [Header("Search Function")]
    public string query;
    public Button search_searchButton;
    public Button results_searchButton;

    void Start()
    {
        results = new List<DatabaseEntry>();
        search_searchButton.onClick.AddListener(SearchFor);
        results_searchButton.onClick.AddListener( SearchFor);
        errorScreen.SetActive(false);
    }

    protected new void Update()
    {
        base.Update();

        if (currentElement == searchPanel)
            query = inputField_search.text;
        if (currentElement == resultsPanel)
            query = inputField_results.text;

        if (Input.GetKeyDown(KeyCode.Return))
            SearchFor();
    }

    public void SetQuery(string _query)
    {
        query = _query;
    }

    public void SearchFor()
    {
        if (currentElement == searchPanel)
            inputField_results.text = query;

        if (query == "")
            return;

        ClearSearch();

        for (int i = 0; i < database.ENTRIES.Count; i++)
        {
            if (database.ENTRIES[i].keywords.Contains(query, StringComparison.OrdinalIgnoreCase))
            {
                results.Add(database.ENTRIES[i]);
            }
        }

        if (results.Count < 1)
            errorScreen.SetActive(true);

        GenerateResults();
        GoToResults();
    }

    public void GenerateResults()
    {
        inputField_search.text = query;
        inputField_results.text = query;

        scrollbar.value = 1;
        foreach (DatabaseEntry r in results)
        {
            GameObject result = Instantiate(resultPrefab, resultsParent);
            

            ResultUI ui = result.GetComponent<ResultUI>();
            ui.AddEntry(r);

            ResetTransform(resultsParent);
        }
    }

    public void ClearSearch()
    {
        if(errorScreen.activeSelf)
            errorScreen.SetActive(false);

        foreach (Transform child in resultsParent)
            GameObject.Destroy(child.gameObject);

        results.Clear();
        ////inputField_search.text = "";
        ////inputField_results.text = "";
    }

    public void GoToResults()
    {
        GoToElement(resultsPanel);
    }

    public void GoToSearch()
    {
        GoToElement(searchPanel);
    }

    void ResetTransform(Transform tr)
    {
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(tr.GetComponent<RectTransform>());
    }
}
