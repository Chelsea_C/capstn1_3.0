﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SceneHandler))]
public class MainMenu : MonoBehaviour
{
    [Header("Buttons")]
    public Button play;
    public Button quit;

    [Header("Audio")]
    public AudioSource BGM;
    public AudioSource mouseClicker;
    public AudioClip bgmClip;
    public AudioClip mouseClickClip;

    public Transition transition;

    void Start()
    {
        play.onClick.AddListener(PlayButtonActivate);
        quit.onClick.AddListener(QuitButtonActivate);

        BGM.clip = bgmClip;
        BGM.loop = true;
        StartCoroutine(AudioController.FadeIn(BGM, 2f));

        mouseClicker.clip = mouseClickClip;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
            mouseClicker.Play();
    }

    void PlayButtonActivate()
    {
        StartCoroutine(AudioController.FadeOut(BGM, 2f));
        transition.nextScene = "Game_Managers";
        transition.TransitionToNextScene(false);

        //transition.TransitionToNextScene(true);
        //GameManager.instance.UpdateDisplay(GameState.PLAY);
    }

    void QuitButtonActivate()
    {
        GetComponent<SceneHandler>().Quit();
    }
}
