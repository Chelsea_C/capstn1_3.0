﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Scrollbar))]
public class ScreenUnlocker : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    public float value;

    public void OnBeginDrag(PointerEventData data)
    {
    }

    public void OnDrag(PointerEventData data)
    {
    }

    public void OnEndDrag(PointerEventData data)
    {
        value = GetComponent<Scrollbar>().value;

        if (value < 0.8)
            GetComponent<Scrollbar>().value = 0;
        else
        {
            GetComponent<Scrollbar>().value = 1;
            LockScreen.instance.Unlock();
        }
    }
}
