﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    public string nextScene;
    public Color loadToColor = Color.white;

    public void TransitionToNextScene(bool loadAdditively)
    {
        if (!SceneManager.GetSceneByName(nextScene).isLoaded)
            Initiate.Fade(nextScene, loadToColor, 1.0f, loadAdditively);
        else
            Debug.Log("Scene is already loaded.");
    }
}
