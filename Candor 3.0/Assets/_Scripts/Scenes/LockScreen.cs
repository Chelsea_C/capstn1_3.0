﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockScreen : MonoBehaviour
{

    #region Singleton
    public static LockScreen instance;

    public void Awake()
    {
        instance = this;
    }
    #endregion

    public ScreenUnlocker slider;
    public CameraBehaviour cam;
    public CanvasGroup cg;

    void Start()
    {
        GameManager.instance.LOCK_SCREEN = this;
        Vibrate();
        cg = GetComponent<CanvasGroup>();
    }

    public void Vibrate()
    {
        AudioManager.instance.vibrate.Play();
        CameraBehaviour.instance.Vibrate();
        CameraBehaviour.instance.Vibrate();
        
    }

    public void Reset()
    {
        slider.value = 0;
        slider.GetComponent<Scrollbar>().value = 0;
    }

    public void Unlock()
    {
        Debug.Log("Unlocked screen!");
        LevelManager.instance.StartLevel(GameManager.instance.CURRENT_LEVEL);
        Hide();
    }

    public void Show()
    {
        cg.alpha = 1;
        cg.interactable = true;
        cg.blocksRaycasts = true;
    }

    public void Hide()
    {
        cg.alpha = 0;
        cg.interactable = false;
        cg.blocksRaycasts = false;
        Reset();
    }

}
