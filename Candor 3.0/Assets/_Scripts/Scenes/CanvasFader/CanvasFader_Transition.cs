﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFader_Transition : CanvasFader
{
    void Start()
    {
        GameManager.instance.fader_transition = this;
        DisableInteraction();
    }

}
