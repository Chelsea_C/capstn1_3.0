﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFader_TimeLoss : CanvasFader
{
    void Start()
    {
        GameManager.instance.fader_timeloss = this;
        DisableInteraction();
    }
    
}
