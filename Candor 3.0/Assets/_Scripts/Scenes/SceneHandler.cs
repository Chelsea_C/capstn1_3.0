﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public void Quit()
    {
        Debug.Log("Exiting game");
        Application.Quit();
    }

    public void RemoveScene(string name)
    {
        SceneManager.UnloadSceneAsync(name);
    }

    public void RemoveScene(int index)
    {
        SceneManager.UnloadSceneAsync(index);
    }

    public void LoadScene(string name)
    {
        Scene scene = SceneManager.GetSceneByName(name);
        if((scene != null) && !scene.isLoaded)
            SceneManager.LoadScene(name, LoadSceneMode.Additive);
    }
}

