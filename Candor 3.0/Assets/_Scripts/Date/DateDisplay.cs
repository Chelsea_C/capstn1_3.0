﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class DateDisplay : MonoBehaviour
{
    public bool ShowDaysElapsed;
    public bool ShowWeekday;
    public bool ShowYear;

    TMP_Text display;

    void Start()
    {
        display = GetComponent<TMP_Text>();
    }

    void Update()
    {
        display.text = DateManager.instance.MONTH_DAY_DISPLAY_TEXT;

        if (ShowDaysElapsed)
        {
            //GetComponent<TMP_Text>().text += " " + DateManager.instance.DAYS_ELAPSED_TEXT;
        }
    }
}


