﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DateManager : MonoBehaviour
{
    #region Singleton
    public static DateManager instance;

     void Awake()
    {
        instance = this;
    }
    #endregion

    public DateTime START_DATE;
    public DateTime END_DATE;

    public DateTime currentDate;

    public string start;
    public string end;
    public string current;
    public string remaining;
    public string elapsed;

    /* Variables */
    public DateTime CURRENT_DATE
    {
        get { return currentDate; }
        set { currentDate = value; } 
    }
    public TimeSpan DIFFERENCE
    {
        get { return CURRENT_DATE - START_DATE; }
    }
    public int DAYS_LEFT
    {
        get
        {
            TimeSpan t = END_DATE - CURRENT_DATE;
            return (int)t.TotalDays;
        }
    }
    public int DAYS_ELAPSED
    {
        get
        {
            TimeSpan t = CURRENT_DATE - START_DATE;
            return (int)t.TotalDays;
        }
    }
    public string MONTH_DAY_DISPLAY_TEXT
    {
        get
        {
            return CURRENT_DATE.ToString("MMMM dd");
        }
    }

    /* Methods */

    void Update()
    {
        start = START_DATE.ToString();
        end = END_DATE.ToString();
        current = CURRENT_DATE.ToString();
        remaining = DAYS_LEFT.ToString();
        elapsed = DAYS_ELAPSED.ToString();
    }

    public void GoToNextDay()
    {
        CURRENT_DATE = CURRENT_DATE.AddDays(1);
    }

    public void GoForwardMinutes(float minutes)
    {
        currentDate.AddMinutes(minutes);
    }

}
