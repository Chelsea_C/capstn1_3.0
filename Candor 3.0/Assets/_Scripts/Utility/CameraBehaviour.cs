﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CameraBehaviour : MonoBehaviour
{
    
    public static CameraBehaviour instance;
    void Awake()
    {

        instance = this;
    }

    public Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Vibrate()
    {
        Debug.Log("Camera vibrating!");
        anim.SetTrigger("Shake");
    }
}
