﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class LevelSummary
{
    public int LEVEL_NUMBER;
    public float TIME_REMAINING;

    public int TOTAL_REVIEWS;
    public int TOTAL_CORRECT_REVIEWS;

    public float INIT_ACCURACY;
    public float SUBMISSION_ACCURACY;
    public float FINAL_ACCURACY;

    public float REWARDS_EARNED;
    public float MAX_REWARD;

    public LevelGrade GRADE = null;

    public List<PlayerReview> REVIEWS;

    public bool HasBeenAddedToData = false;

    public LevelSummary(int level_number, List<PlayerReview> reviews)
    {
        REVIEWS                 = reviews;
        LEVEL_NUMBER            = level_number;

        INIT_ACCURACY           = GameManager.instance.PLAYER_DATA.REVIEW_ACCURACY;
        TOTAL_REVIEWS           = reviews.Count;
        TOTAL_CORRECT_REVIEWS   = GetCorrectReviews();

        SUBMISSION_ACCURACY = CalculateSubmissionsAccuracy();
        FINAL_ACCURACY      = CalculateFinalAccuracy();
        GRADE               = CalculateGrade();
        REWARDS_EARNED      = CalculateRewards();
        MAX_REWARD          = CalculateMaxReward();
    }

    int GetCorrectReviews()
    {
        int correct_reviews = 0;
        for (int i = 0; i < REVIEWS.Count; i++)
            if (REVIEWS[i].RESULT == ReviewResult.CORRECT_RATINGS)
                correct_reviews++;
        return correct_reviews;
    }

    float CalculateSubmissionsAccuracy()
    {
        if (TOTAL_REVIEWS == 0)
            return 0;

        return (float)TOTAL_CORRECT_REVIEWS / (float)TOTAL_REVIEWS * 100f;
    }

    public float CalculateFinalAccuracy()
    {
        if (GameManager.instance.PLAYER_DATA.TOTAL_REVIEWS == 0)
            return 0;

        var total =GameManager.instance.PLAYER_DATA.TOTAL_REVIEWS;
        var correct = GameManager.instance.PLAYER_DATA.CORRECT_REVIEWS;
        
        return  (float)correct / (float)total * 100f;
    }

    LevelGrade CalculateGrade()
    {
        if (SUBMISSION_ACCURACY < 0)
            return null;

        foreach (LevelGrade lg in LevelManager.instance.grades)
        {
            if (SUBMISSION_ACCURACY.Between(lg.minPercent, lg.maxPercent, true))
                return lg;
        }
        return null;
    }

    float CalculateRewards()
    {
        if (GRADE == null)
            return 0;

        var totalReviews = (float)TOTAL_REVIEWS;
        var totalCorrectReviews = (float)TOTAL_CORRECT_REVIEWS;

        var perReview = LevelManager.instance.rewardPerReview;
        float rewards = (totalReviews - totalCorrectReviews) * perReview;
        rewards += totalCorrectReviews * perReview * GRADE.multiplier;
        return rewards;
    }

    float CalculateMaxReward()
    {
        return (float)TOTAL_REVIEWS * LevelManager.instance.HIGHEST_MULTIPLIER * LevelManager.instance.rewardPerReview;
    }
}
