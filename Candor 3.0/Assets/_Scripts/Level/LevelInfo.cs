﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelInfo
{
    [Header("UI")]
    public int START_HOUR;
    public int START_MINUTES;
    public float DURATION;
    public Sprite BACKGROUND;
    public AudioClip BGM;

    [Header("Article Generation")]
    public int NUMBER_OF_ARTICLES;

    [Header("Rewards")]
    public float REWARD_PER_REVIEW;
}