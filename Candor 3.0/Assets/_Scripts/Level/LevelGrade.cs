﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Grade
{
    A,
    B,
    C,
    D,
    F
}

[System.Serializable]
public class LevelGrade
{
    public string letter_text;
    public string feedback_text;
    public float minPercent;
    public float maxPercent;
    public Grade letter_enum;
    public float multiplier;

    public LevelGrade(Grade _letter, string _text, float _minPercent, float _maxPercent, string _feedback, float _multiplier = 1)
    {
        letter_enum = _letter;
        letter_text = _text;
        minPercent = _minPercent;
        maxPercent = _maxPercent;
        multiplier = _multiplier;
        feedback_text = _feedback;
    }
}
