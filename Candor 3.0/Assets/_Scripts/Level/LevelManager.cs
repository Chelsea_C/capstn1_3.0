﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class LevelManager : MonoBehaviour
{
    #region Singleton
    public static LevelManager instance;

    public void Awake()
    {
        instance = this;
    }
    #endregion

    public GameData database;
    public LevelData levelData;
    public Background bg;

    [Header("Level Info")]
    public List<LevelGrade> grades;
    public List<Rule> generalRules;
    public List<Rule> submissionRules;

    [Header("Level Summary")]
    public float rewardPerReview;

    [Header("Current")]
    public List<Article> currentArticles;
    public LevelSummary currentLevelSummary;
    public LevelInfo currentLevelInfo;
    public bool IsAllowingSubmissions;

    public int level_index;
    
    public float HIGHEST_MULTIPLIER
    {
        get
        {
            float highestGradeMultiplier = 0;
            if (rewardPerReview > 0)
            {
                foreach (LevelGrade lg in grades)
                {
                    if (lg.multiplier >= highestGradeMultiplier)
                        highestGradeMultiplier = lg.multiplier;
                }
            }
            return highestGradeMultiplier;
        }
    }

    bool emptyLevelActive = false;

    public void Start()
    {
        database = GameManager.instance.DATA;
        levelData = GameManager.instance.LEVELS;
        
        #region Setup of Grades
        grades = new List<LevelGrade>();
        LevelGrade A = new LevelGrade(Grade.A, "A", 95, 100, "Accuracy is between 95 and 100 percent.", 2f);
        LevelGrade B = new LevelGrade(Grade.B, "B", 85, 94, "Accuracy is between 85 and 94 percent.", 1.5f);
        LevelGrade C = new LevelGrade(Grade.C, "C", 60, 84, "Accuracy is between 60 and 84 percent.");
        LevelGrade D = new LevelGrade(Grade.D, "D", 40, 59, "Accuracy is between 40 and 59 percent.");
        LevelGrade F = new LevelGrade(Grade.F, "F", 0, 39, "Accuracy is between 0 and 39 percent.");

        grades.Add(A);
        grades.Add(B);
        grades.Add(C);
        grades.Add(D);
        grades.Add(F);

        #endregion

        emptyLevelActive = false;
    }

    #region Level Setup

    public void StartLevel(int level_number)
    {
        Debug.Log("Starting Level: " + level_number);

        if (level_number == 0)
        {
            LaunchEmptyLevel();
            return;
        }

        #region LevelInfo null check
        if (level_number > levelData.LEVELS.Count || levelData.LEVELS[level_number - 1] == null)
        {
            Debug.LogError("No level information for level " + level_number + " found.");
            GameManager.instance.EndGame(GameEnding.LOSE_TIME);
            return;
        }
        #endregion

        level_index = level_number - 1;
        LevelInfo currentLevel = levelData.LEVELS[level_index];
        currentLevelInfo = currentLevel;

        AudioManager.instance.bgmClip = currentLevel.BGM;
        StartCoroutine(AudioController.FadeIn(AudioManager.instance.bgm, 5f, 0.6f));

        // Clear current articles list
        currentArticles.Clear();

        // Set up timer
        var timeMgr = TimeManager.instance;
        if(timeMgr.IsTimerActive)
            timeMgr.IsTimerActive = false;
        timeMgr.SetTimer(currentLevel.DURATION);
        timeMgr.StartTimer();

        // Set up background
        bg.SetBackgroundImage(currentLevel.BACKGROUND);

        // Set up articles
        AddArticles(level_index);

        Hooter.instance.OnCandorBubbleDeactivate();
        //NotificationManager.instance.CreateNotification(Candor.instance, "Candor submissions are open for a limited time!");
        //NotificationManager.instance.CreateNotification(Hooter.instance, "New Hoots available");

        IsAllowingSubmissions = true;
    }

    public void LaunchEmptyLevel()
    {
        emptyLevelActive = true;

    }

    public void AddTutorialArticle(Article a)
    {
        currentArticles.Clear();
        currentArticles.Add(a);
        PublishHooterPosts();
    }

    void AddArticles(int level_index)
    {
        LevelInfo currentLevel = levelData.LEVELS[level_index];

        int count = 0;
        int quota = currentLevel.NUMBER_OF_ARTICLES;
        bool quotaReached = false;

        while (!quotaReached)
        {
            int randomIndex = Random.Range(0, database.ARTICLES.Count);
            var article = database.ARTICLES[randomIndex];
            Debug.Log(article.id);

            if (!article.IsActiveInLevel && !article.HasBeenReviewed)
            {
                article.IsActiveInLevel = true;
                currentArticles.Add(article);
                count++;
            }

            if(count == quota)
                quotaReached = true;
        }
        
        PublishHooterPosts();
    }

    void PublishHooterPosts()
    {
        foreach (Article active in currentArticles)
            Hooter.instance.CreatePost(active);
    }
    #endregion

    #region Level Summary

    public void EndLevel()
    {
        if(emptyLevelActive)
        {
            emptyLevelActive = false;
            GameManager.instance.SetUpNextLevel();
            return;
        }


        Debug.Log("Ending level: " + GameManager.instance.CURRENT_LEVEL);

        IsAllowingSubmissions = false;
        StartCoroutine(AudioController.FadeOut(AudioManager.instance.bgm, 3));

        NotificationManager.instance.EndLevel();
        //NotificationManager.instance.CreateNotification(Candor.instance, "Submissions closed. Thank you to all who contributed!");
        
        var candor_reviews = Candor.instance.level_submissions;
                     
        // Create new level summary
        LevelSummary currentSummary = new LevelSummary(level_index + 1, candor_reviews);

        // Add summary data to player data
        Player.instance.UpdateData(currentSummary);

        // Calculate new accuracy
        currentSummary.FINAL_ACCURACY = currentSummary.CalculateFinalAccuracy();

        // Create email of level summary
        MailManager.instance.CreateCandorMail(DateManager.instance.CURRENT_DATE, currentSummary);

        // Clear variables
        Candor.instance.EndLevel();
        Hooter.instance.EndLevel();
        
        ClearCurrentArticles();
        //currentSummary = null;

        // Check if win or lose
        GameManager.instance.CheckEndCondition(currentSummary.SUBMISSION_ACCURACY);
    }

    void ClearCurrentArticles()
    {
        foreach(Article a in currentArticles)
        {
            a.IsActiveInLevel = false;
        }

        currentArticles.Clear();
    }
    #endregion
}
