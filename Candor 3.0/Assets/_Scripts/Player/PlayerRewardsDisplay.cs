﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


[RequireComponent(typeof(TMP_Text))]
public class PlayerRewardsDisplay : MonoBehaviour
{
    
    TMP_Text display;

    void Start()
    {
        display = GetComponent<TMP_Text>();
    }

    void Update()
    {
        display.text = Player.instance.data.str_amount_rewards;
    }
}

