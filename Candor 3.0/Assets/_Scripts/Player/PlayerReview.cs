﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InfoRating
{
    NULL = 999,
    FALSE = 0,
    TRUE = 1, 
    MIXTURE = 2,
    UNPROVEN = 3
}

public enum SourceRating
{
    NULL = 999,
    UNCITED = 0,
    VALIDATED = 1,
    NONVALIDATED = 2
}

public enum ReviewResult
{
    NULL = 0,
    CORRECT_RATINGS = 1,
    WRONG_SOURCE_RATING = 2,
    WRONG_INFO_RATING = 3,
    WRONG_RATINGS = 4
}

[System.Serializable]
public class PlayerReview
{
    public Article article;
    public InfoRating infoRating;
    public SourceRating sourceRating;
    public ReviewResult RESULT
    {
        get
        {
            if (article.INFO_RATING == infoRating && article.SOURCE_RATING == sourceRating)
            {
                return ReviewResult.CORRECT_RATINGS;
            }

            if (article.INFO_RATING != infoRating && article.SOURCE_RATING == sourceRating)
            {
                return ReviewResult.WRONG_INFO_RATING;
            }

            if (article.INFO_RATING == infoRating && article.SOURCE_RATING != sourceRating)
            {
                return ReviewResult.WRONG_SOURCE_RATING;
            }

            if (article.INFO_RATING != infoRating && article.SOURCE_RATING != sourceRating)
            {
                return ReviewResult.WRONG_RATINGS;
            }

            return ReviewResult.NULL;
        }
    }
    public PlayerReview(Article _article, InfoRating _ir = InfoRating.NULL, SourceRating _sr = SourceRating.NULL)
    {
        article = _article;
        infoRating = _ir;
        sourceRating = _sr;
    }



}
