﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    #region Singleton

    public static Player instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public PlayerData data;
    public float accuracy;
    public string accuracyText;
    public float rewards;
    public string rewardsText;
    public float maxRewards;
    public string maxRewardsText;
    public float totalReviews;
    public float correctreviews;
    

    void Start()
    {
        data.ResetAll();
        if (data == null)
            data = GameManager.instance.PLAYER_DATA;

        data.REVIEWS = new List<PlayerReview>();
        data.SUMMARIES = new List<LevelSummary>();
    }

    void Update()
    {
        accuracy = data.REVIEW_ACCURACY;
        accuracyText = data.REVIEW_ACCURACY.ToString("F1") + "%";
        rewards = data.AMOUNT_REWARDS;
        rewardsText = data.AMOUNT_REWARDS.ToString("F1");
        maxRewards = data.AMOUNT_MAX_REWARDS;
        maxRewardsText = data.AMOUNT_MAX_REWARDS.ToString("F1");
        totalReviews = data.TOTAL_REVIEWS;
        correctreviews = data.CORRECT_REVIEWS;
    }

    public void UpdateData(LevelSummary level)
    {
        if (level.HasBeenAddedToData)
            return;
        level.HasBeenAddedToData = true;

        Debug.Log("Updating player data");
        data.AMOUNT_REWARDS += level.REWARDS_EARNED;
        data.AMOUNT_MAX_REWARDS += level.MAX_REWARD;
        data.SUMMARIES.Add(level);

        foreach (PlayerReview pr in level.REVIEWS)
            data.REVIEWS.Add(pr);
    }
}