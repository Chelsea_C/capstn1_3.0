﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(TMP_Text))]
public class PlayerAccuracyDisplay : MonoBehaviour
{
    TMP_Text display;

    void Start()
    {
        display = GetComponent<TMP_Text>();
    }

    void Update()
    {
        var summaries = Player.instance.data.SUMMARIES;
        if (summaries.Count < 1)
            return;
        int lastLevel = summaries.Count;
        display.text = summaries[lastLevel - 1].SUBMISSION_ACCURACY.ToString("F1") + "%";
    }
}
