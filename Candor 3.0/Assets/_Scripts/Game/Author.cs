﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Author
{
    public string name;
    public Sprite portrait;

    public Author(string _name, string _portraitPath)
    {
        name = _name;

        string portraitPath = "Authors/" + _portraitPath;
        portrait = Resources.Load<Sprite>(portraitPath);
    }

    public Author(string _name, Sprite _portrait )
    {
        name = _name;
        portrait = _portrait;
    }
}