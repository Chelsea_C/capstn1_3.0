﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tag
{
    public int id;
    public string text;

    public Tag(int _id, string _text)
    {
        id = _id;
        text = _text;
    }
}
