﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RulesManager : MonoBehaviour
{
    #region Singleton
    public static RulesManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public List<Rule> rules;

    void Start()
    {
        GetRules();
    }

    void GetRules()
    {
        rules = new List<Rule>();
        Rule[] _rules = GetComponentsInChildren<Rule>();
        for (int i = 0; i < _rules.Length; i++)
            rules.Add(_rules[i]);
    }



}
