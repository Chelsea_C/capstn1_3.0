﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlayerData", menuName = "ScriptableObject/PlayerData")]
public class PlayerData : ScriptableObject
{
    [Header("Profile Info")]
    public new string name;
    public Sprite playerPortrait;

    [Header("Stats")]
    public bool CompletedCandorTutorial;
    public List<PlayerReview> REVIEWS;
    public List<LevelSummary> SUMMARIES;
    
    public float amountRewards;
    public float amountMaxRewards;
    public bool HasBeenWarned = false;
    
    // REVIEWS
    public int TOTAL_REVIEWS { get { return REVIEWS.Count; } }
    public int CORRECT_REVIEWS
    {
        get {
            int count = 0;
            for (int i = 0; i < REVIEWS.Count; i++)
                if (REVIEWS[i].RESULT == ReviewResult.CORRECT_RATINGS)
                    count++;
            return count;
        }
    }
    public int INCORRECT_REVIEWS
    {
        get {
            int count = 0;
            for (int i = 0; i < REVIEWS.Count; i++)
                if (REVIEWS[i].RESULT != ReviewResult.CORRECT_RATINGS)
                    count++;
            return count;
        }
    }
    public float REVIEW_ACCURACY
    {
        get
        {
            if (REVIEWS.Count < 1)
                return 0f;

            int count = 0;
            foreach (PlayerReview r in REVIEWS)
                if (r.RESULT == ReviewResult.CORRECT_RATINGS)
                    count++;

            return ((float)count / (float)REVIEWS.Count) * 100f;
        }
    }

    // REWARDS
    public float AMOUNT_REWARDS { get { return amountRewards; } set { amountRewards = value; } }
    public float AMOUNT_MAX_REWARDS { get { return amountMaxRewards; } set { amountMaxRewards = value; } }
    public float AMOUNT_LOSS { get { return amountMaxRewards - amountRewards; } }

    // STRINGS
    public string str_total_reviews { get { return TOTAL_REVIEWS.ToString(); } }
    public string str_total_correct { get { return CORRECT_REVIEWS.ToString(); } }
    public string str_total_incorrect { get { return INCORRECT_REVIEWS.ToString(); } }

    public string str_review_accuracy { get { return REVIEW_ACCURACY.ToString("F1") + "%"; } }
    public string str_amount_rewards { get { return "$" + AMOUNT_REWARDS.ToString("F2"); } }
    public string str_amount_max_rewards { get { return "$" + AMOUNT_MAX_REWARDS.ToString("F2"); } }
    public string str_amount_loss { get { return "$" + AMOUNT_LOSS.ToString("F2"); } }

    public void ResetAll()
    {
        CompletedCandorTutorial = false;

        amountRewards = 0;
        amountMaxRewards = 0;
        HasBeenWarned = false;

        REVIEWS.Clear();
    }
}
