﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DATABASES;

[CreateAssetMenu(fileName = "New GameData", menuName = "ScriptableObject/GameData")]
public class GameData : ScriptableObject
{
    public List<Author> AUTHORS;
    public List<Article> ARTICLES;
    public List<DatabaseEntry> ENTRIES;
    public List<Tag> TAGS;
    public List<Source> SOURCES;

    public void ClearAll()
    {
        AUTHORS.Clear();
        ARTICLES.Clear();
        ENTRIES.Clear();
        TAGS.Clear();
        SOURCES.Clear();
    }

    public int AVAILABLE_ARTICLES
    {
        get
        {
            int count = 0;
            foreach (Article a in ARTICLES)
                if (!a.IsActiveInLevel && !a.HasBeenReviewed)
                    count++;
            return count;
        }
    }
}
