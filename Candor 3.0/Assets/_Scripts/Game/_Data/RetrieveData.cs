﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;

namespace DATABASES
{
    public class RetrieveData : MonoBehaviour
    {
        public GameData database;

        public void Start()
        {
            database.ClearAll();

            //Debug.Log(Application.dataPath + "/CandorDB.s3db");
            string conn = "URI=file:" + Application.dataPath + "/Resources/SQL/CandorDB.s3db";
            IDbConnection dbconn;
            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open();

            GetDatabaseEntries(dbconn);
            GetAuthors(dbconn);
            GetTags(dbconn);
            GetSources(dbconn);

            GetArticles(dbconn);

            dbconn.Close();
            dbconn = null;
        }

        void GetDatabaseEntries(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
                "IFNULL(Keywords, ''), " +
                "IFNULL(URL, ''), " +
                "IFNULL(Heading, ''), " +
                "IFNULL(Content, ''), " +
                "IFNULL(LogoPath, '') " + "FROM DATABASE";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                string keywords = reader.GetString(0);
                string url = reader.GetString(1);
                string heading = reader.GetString(2);
                string content = reader.GetString(3);
                string logoPath = reader.GetString(4);

                DatabaseEntry entry = new DatabaseEntry(keywords, url, heading, content, logoPath);
                database.ENTRIES.Add(entry);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetInformationRatings(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
                "ID, " +
                "IFNULL(Description, '') " + "FROM INFORMATIONRATINGS";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string description = reader.GetString(1);
                
                // error = new Error(id, description);
                //database.ERRORS.Add(error);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetArticles(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
                "ID, " +
                "InformationRating, " +
                "IFNULL(Author, ''), " +
                "IFNULL(Content, ''), " +
                "IFNULL(LinkedContent, ''), " +
                "IFNULL(Source, ''), " +
                "IFNULL(Keywords, ''), " +
                "IFNULL(Tags, '') " + "FROM ARTICLES";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            { 
                int id = reader.GetInt32(0);
                int informationRating = reader.GetInt32(1);
                string author = reader.GetString(2);
                string content = reader.GetString(3);
                string linkedContent = reader.GetString(4);
                string source = reader.GetString(5);
                string keywords = reader.GetString(6);
                string tags = reader.GetString(7);

                Article article = new Article(id, informationRating, author, content, linkedContent, source, keywords, tags);
                database.ARTICLES.Add(article);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetAuthors(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
                "Name, " +
                "PortraitPath " + "FROM AUTHORS";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                string  name = reader.GetString(0);
                string portraitPath = reader.GetString(1);

                Sprite portrait = Resources.Load<Sprite>("Authors/" + portraitPath);

                Author author = new Author(name, portrait);
                database.AUTHORS.Add(author);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetTags(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
                "ID, " +
                "IFNULL(Text, '') " + "FROM TAGS";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string text = reader.GetString(1);

                Tag tag = new Tag(id, text);
                database.TAGS.Add(tag);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }
        
        void GetSources(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT " +
            "ID, " +
            "Credibility, " +
            "IFNULL(Text, ''), " +
            "IFNULL(LogoPath, '') " + "FROM SOURCES";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                int credibility = reader.GetInt32(1);
                string text = reader.GetString(2);
                string logoPath = reader.GetString(3);

                Source source = new Source(id, credibility, text, logoPath);
                database.SOURCES.Add(source);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }
    }
}
