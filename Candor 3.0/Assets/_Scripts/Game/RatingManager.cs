﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InfoRatingInfo
{
    public InfoRating value;
    public string description;
}

[System.Serializable]
public class SourceRatingInfo
{
    public SourceRating value;
    public string description;
}

public class RatingManager : MonoBehaviour
{
    #region Singleton
    public static RatingManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public List<SourceRatingInfo> SOURCE_RATINGS;
    public List<InfoRatingInfo> INFO_RATINGS;

    public string GetText(SourceRating sr)
    {
        foreach(SourceRatingInfo sri in SOURCE_RATINGS)
            if(sri.value == sr)
                return sri.description;
        return string.Empty;
    }

    public string GetText(InfoRating ir)
    {
        foreach (InfoRatingInfo iri in INFO_RATINGS)
            if (iri.value == ir)
                return iri.description;
        return string.Empty;
    }

}
