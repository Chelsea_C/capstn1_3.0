﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Source
{
    public int id;
    public int credibility;
    public string text;
    public Sprite logo;

    public Source(int _id, int _credibility, string _text, string _logoPath)
    {
        id = _id;
        credibility = _credibility;
        text = _text;

        string logoPath = "Websites/" + _logoPath;
        logo = Resources.Load<Sprite>(logoPath);
    }

    public Source(int _id, int _credibility, string _text, Sprite _logo)
    {
        id = _id;
        credibility = _credibility;
        text = _text;
        logo = _logo;
    }
}
