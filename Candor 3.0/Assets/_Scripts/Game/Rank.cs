﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Rank
{
    private readonly string name;
    private readonly int value;

    public static readonly Rank JUNIOR_ANALYST = new Rank(0, "Junior Analyst");
    public static readonly Rank SENIOR_ANALYST = new Rank(1, "Senior Analyst");
    public static readonly Rank FACT_SPECIALIST = new Rank(2, "Fact Specialist");
    public static readonly Rank TRUST_AFICIONADO = new Rank(3, "Trust Aficianado");
    public static readonly Rank TRUTH_CONNOISEUR = new Rank(4, "Truth Connoiseur");
    public static readonly Rank CANDID_CHAMPION = new Rank(5, "Candid Champion");

    private Rank(int value, string name)
    {
        this.name = name;
        this.value = value;
    }

    public override string ToString()
    {
        return name;
    }

    private static readonly Dictionary<string, Rank> instance = new Dictionary<string, Rank>();
}