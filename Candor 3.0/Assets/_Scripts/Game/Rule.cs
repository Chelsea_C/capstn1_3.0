﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Rule: MonoBehaviour
{
    public int id;
    public string description;
    public bool IsActive;
    public bool IsViolated;
    public GameObject uiPrefab;

    CandorRuleUI ui;

    public Rule(int _id, string _description)
    {
        id = _id;
        description = _description;
        ui = uiPrefab.GetComponent<CandorRuleUI>();
        ui.description.text = _description;
    }

    public virtual void CheckIfViolated(Article article)
    {

    }

    public bool SearchFor(Article _article, Tag _tag)
    {
        bool hasTag = false;
        foreach (int n in _article.tags)
        {
            if (n == _tag.id)
            {
                hasTag = true;
            }
        }

        return hasTag;
    }
}
