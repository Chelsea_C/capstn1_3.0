﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    MENU = 0,
    PLAY = 1,
    PAUSED = 2
}

public enum Month
{
    NULL, JAN, FEB, MAR, APR, MAY,
    JUN, JUL, AUG, SEP, OCT, NOV, DEC
}

public enum GameEnding
{
    LOSE_ACCURACY = 0,
    WIN = 1,
    LOSE_TIME = 2
}

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    public GameData DATA;
    public PlayerData PLAYER_DATA;
    public LevelData LEVELS;
    public GameState STATE = GameState.MENU;

    [Header("Game Goals")]
    public float GOAL_MONEY;
    public float END_ACCURACY;

    [Header("Start Date")]
    [Range(1, 12)]
    public int start_month;
    [Range(1, 31)]
    public int start_day;
    [Range(2013, 2018)]
    public int start_year;

    [Header("Deadline Date")]
    [Range(1, 12)]
    public int end_month;
    [Range(1, 31)]
    public int end_day;
    [Range(2013, 2018)]
    public int end_year;

    [Header("Time")]
    public float SECONDS_PER_INGAME_MINUTE = 60;

    [Header("Levels")]
    public int CURRENT_LEVEL = 0;

    [Header("Scene")]
    public string menuScene = "Game_Menu";
    public string displayScene = "Game_Display";
    public CanvasFader fader_transition;
    public CanvasFader fader_win;
    public CanvasFader fader_accuracyloss;
    public CanvasFader fader_timeloss;
    public float timeBetweenFades;
    public LockScreen LOCK_SCREEN;

    public void Start()
    {
        SetDates();
        InitLevel();
        UpdateDisplay(GameState.PLAY);
    }

    void Update()
    {

    }

    public void UpdateDisplay(GameState state)
    {
        if (state == GameState.MENU)
        {
            if (!SceneManager.GetSceneByName("Game_Menu").isLoaded)
            {
                SceneManager.LoadScene("Game_Menu", LoadSceneMode.Additive);
            }
        }

        if (state == GameState.PLAY)
        {
            if (!SceneManager.GetSceneByName("Game_Display").isLoaded)
            {
                SceneManager.LoadScene("Game_Display", LoadSceneMode.Additive);
            }
        }
    }

    void SetDates()
    {
        DateTime startdate = new DateTime(start_year, start_month, start_day);
        DateManager.instance.START_DATE = startdate;
        DateManager.instance.CURRENT_DATE = startdate;

        DateTime enddate = new DateTime(end_year, end_month, end_day);
        DateManager.instance.END_DATE = enddate;

        if (DateManager.instance.DAYS_LEFT != LEVELS.LEVELS.Count)
            Debug.LogError("There are not enough levels defined!");

    }

    void InitLevel()
    {
        CURRENT_LEVEL = 0;
        TimeManager.instance.SetTimeTo(LEVELS.LEVELS[CURRENT_LEVEL].START_HOUR, LEVELS.LEVELS[CURRENT_LEVEL].START_MINUTES, 0);
    }

    public void CheckEndCondition(float submissionAccuracy)
    {
        Debug.Log("Checking win condition....");

        //if player loses because of accuracy
        if (submissionAccuracy < 60f)
        {
            if (PLAYER_DATA.HasBeenWarned)
            {
                EndGame(GameEnding.LOSE_ACCURACY);
                return;
            }
            else
                PLAYER_DATA.HasBeenWarned = true;
        }


        // if player wins enough money within the deadline
        if (PLAYER_DATA.AMOUNT_REWARDS >= GOAL_MONEY)
        {
            EndGame(GameEnding.WIN);
            return;
        }

        // if it is the deadline
        if(DateManager.instance.DAYS_LEFT <= 0 && PLAYER_DATA.AMOUNT_REWARDS < GOAL_MONEY)
        {
            EndGame(GameEnding.LOSE_TIME);
            return;
        }

        // if there are no more levels
        if (CURRENT_LEVEL >= LEVELS.LEVELS.Count)
        {
            EndGame(GameEnding.LOSE_TIME);
            return;
        }

        DateManager.instance.GoToNextDay();
        SetUpNextLevel();
    }

    public void SetUpNextLevel()
    {
        CURRENT_LEVEL++;
        StartCoroutine(FadeToNextLevel());
    }

    public void EndGame(GameEnding end)
    {
        switch(end)
        {
            case GameEnding.LOSE_ACCURACY:
                fader_accuracyloss.AllowInteraction();
                fader_accuracyloss.FadeIn();
                break;
            case GameEnding.LOSE_TIME:
                fader_timeloss.AllowInteraction();;
                fader_timeloss.FadeIn();
                break;
            case GameEnding.WIN:
               fader_win.AllowInteraction();;
                fader_win.FadeIn();
                break;
            default:
                break;
        }
    }

    IEnumerator FadeToNextLevel()
    {
        fader_transition.FadeIn();
        yield return new WaitForSeconds(timeBetweenFades);
        AppManager.instance.CloseAndResetAll();
        LOCK_SCREEN.Show();
        fader_transition.FadeOut();
        LOCK_SCREEN.Vibrate();
    }

}
