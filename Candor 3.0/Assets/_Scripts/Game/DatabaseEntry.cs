﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATABASES
{
    [System.Serializable]
    public class DatabaseEntry
    {
        public string keywords;
        public string heading;
        public string content;
        public string url;
        public Sprite logo;

        public DatabaseEntry(string _keywords, string _url, string _heading, string _content, string _logoPath)
        {
            keywords = _keywords;
            heading = _heading;
            content = _content;
            url = _url;

            string logoPath = "Websites/" + _logoPath;
            logo = Resources.Load<Sprite>(logoPath);
        }
    }
}