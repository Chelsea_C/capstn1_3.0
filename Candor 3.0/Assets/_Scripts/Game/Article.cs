﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Article
{
    [Header("Retrieved from Database")]
    public int id;
    public int informationRating;
    public string author;
    public string content;
    public string linkedContent;
    public string keywords;

    public Source source;
    public List<int> errors;
    public List<int> tags;

    [Header("Game")]
    public bool IsActiveInLevel;
    public bool HasBeenReviewed;
    public List<Rule> rulesApplied;

    public InfoRating INFO_RATING
    {
        get
        {
            return (InfoRating)informationRating;
        }
    }
    public SourceRating SOURCE_RATING
    {
        get
        {
            if (source != null)
                return (SourceRating)source.credibility;
            else
                return SourceRating.UNCITED;
        }
    }


    public Article(int _id, int _infoRating, string _author, string _content, string _linkedContent,
        string _sources, string _keywords, string _tags)
    {
        id = _id;
        informationRating = _infoRating;
        author = _author;
        keywords = _keywords;

        content = _content;
        linkedContent = _linkedContent;

        IsActiveInLevel = false;
        HasBeenReviewed = false;

        tags = new List<int>();
        GetTags(_tags);

        source = GetSource(_sources);

        rulesApplied = new List<Rule>();
    }

    Source GetSource(string _sources)
    {
        Char separator = ' ';
        if (!string.Equals(_sources, "x"))
        {
            string[] sourcesStrings = _sources.Split(separator);
            foreach (string s in sourcesStrings)
            {
                if (!string.Equals(s, "x"))
                {
                    int code = 0;
                    if (int.TryParse(s, out code))
                    {
                        var sources = LevelManager.instance.database.SOURCES;
                        for (int i = 0; i < sources.Count; i++)
                        {
                            if (sources[i].id == code)
                                return sources[i];
                        }
                    }

                }
            }
        }
        return null;
    }

    void GetTags(string _tags)
    {
        Char separator = ' ';
        if (!string.Equals(_tags, "x"))
        {
            string[] tagStrings = _tags.Split(separator);
            foreach (string s in tagStrings)
            {
                if (!string.Equals(s, "x"))
                {
                    int code = 0;
                    if (int.TryParse(s, out code))
                        tags.Add(code);
                }
            }
        }
    }
}
